package pl.mrozok.drawabletest;

import android.databinding.BindingAdapter;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public final class BindingAdapters {

    @BindingAdapter("android:src")
    public static void setImageResource(ImageView view, @DrawableRes int resource) {
        view.setImageResource(resource);
    }

    @BindingAdapter("glideSrc")
    public static void setImageResourceWithGlide(ImageView view, @DrawableRes int resource) {
        Glide.with(view.getContext())
                .load(resource)
                .into(view);
    }
}
