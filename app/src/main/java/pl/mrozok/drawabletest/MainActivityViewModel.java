package pl.mrozok.drawabletest;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import pl.mrozok.drawabletest.list.ListActivity;

public class MainActivityViewModel {

    private Context context;

    public MainActivityViewModel(Context context) {
        this.context = context;
    }

    public void onListClick(View view) {
        context.startActivity(getListActivityIntent());
    }

    public void onGlideListClick(View view) {
        Intent i = getListActivityIntent();
        ListActivity.putGlideValue(i);
        context.startActivity(i);
    }

    private Intent getListActivityIntent() {
        return new Intent(context, ListActivity.class);
    }
}
