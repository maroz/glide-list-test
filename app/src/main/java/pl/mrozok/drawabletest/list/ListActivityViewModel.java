package pl.mrozok.drawabletest.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import pl.mrozok.drawabletest.R;

public class ListActivityViewModel {

    private static final int[] IMAGE_RESOURCES = {R.drawable.bike_1, R.drawable.bike_2, R.drawable.bike_3, R.drawable.bike_4,
            R.drawable.bike_5, R.drawable.bike_6, R.drawable.bike_7, R.drawable.bike_8,
            R.drawable.bike_9, R.drawable.bike_10, R.drawable.bike_11, R.drawable.bike_12};

    private Context context;

    public ListActivityViewModel(Context context) {
        this.context = context;
    }

    public RecyclerView.Adapter createAdapter(boolean withGlide) {
        if (withGlide)
            return new GlideListRecyclerViewAdapter(context, IMAGE_RESOURCES);
        else
            return new ListRecyclerViewAdapter(context, IMAGE_RESOURCES);
    }
}
