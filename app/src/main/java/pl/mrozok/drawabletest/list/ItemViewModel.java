package pl.mrozok.drawabletest.list;

import android.support.annotation.DrawableRes;

public class ItemViewModel {

    @DrawableRes private int imageRes;

    public ItemViewModel(@DrawableRes int imageRes) {
        this.imageRes = imageRes;
    }

    public int getImageRes() {
        return imageRes;
    }
}
