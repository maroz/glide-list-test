package pl.mrozok.drawabletest.list;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import pl.mrozok.drawabletest.R;
import pl.mrozok.drawabletest.databinding.ActivityListBinding;

public class ListActivity extends AppCompatActivity {

    private static final String ARG_WITH_GLIDE = "glide";
    private static final int COLUMN_NUMBER = 3;

    private ActivityListBinding binding;

    private boolean glide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handleBundle(getIntent());
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list);
        initRecyclerView();
    }

    private void handleBundle(Intent intent) {
        if (null != intent)
            glide = intent.getBooleanExtra(ARG_WITH_GLIDE, false);
    }

    private void initRecyclerView() {
        ListActivityViewModel viewModel = new ListActivityViewModel(this);

        RecyclerView list = binding.list;
        list.setHasFixedSize(true);
        list.setLayoutManager(new GridLayoutManager(this, COLUMN_NUMBER));
        list.setAdapter(viewModel.createAdapter(glide));
    }

    public static void putGlideValue(Intent i) {
        i.putExtra(ARG_WITH_GLIDE, true);
    }
}
