package pl.mrozok.drawabletest.list;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import pl.mrozok.drawabletest.databinding.ItemBinding;

public class ListRecyclerViewAdapter extends Adapter<ListRecyclerViewAdapter.ItemViewHolder> {

    private LayoutInflater inflater;

    private int[] imageResources;

    public ListRecyclerViewAdapter(@NonNull Context context, @NonNull int[] imageResources) {
        this.imageResources = imageResources;
        this.inflater = LayoutInflater.from(context);
        setHasStableIds(true);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ItemViewHolder.create(inflater, parent);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bindTo(imageResources[position]);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return imageResources.length;
    }

    static class ItemViewHolder extends ViewHolder {

        private ItemBinding binding;

        public ItemViewHolder(ItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public static ItemViewHolder create(LayoutInflater inflater, ViewGroup parent) {
            ItemBinding binding = ItemBinding.inflate(inflater, parent, false);
            return new ItemViewHolder(binding);
        }

        public void bindTo(@DrawableRes int imageRes) {
            ItemViewModel vm = new ItemViewModel(imageRes);
            binding.setViewModel(vm);
        }
    }
}
