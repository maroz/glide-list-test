package pl.mrozok.drawabletest.list;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import pl.mrozok.drawabletest.databinding.GlideItemBinding;

public class GlideListRecyclerViewAdapter extends Adapter<GlideListRecyclerViewAdapter.ItemViewHolder> {

    private LayoutInflater inflater;

    private int[] imageResources;

    public GlideListRecyclerViewAdapter(@NonNull Context context, @NonNull int[] imageResources) {
        this.imageResources = imageResources;
        this.inflater = LayoutInflater.from(context);
        setHasStableIds(true);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ItemViewHolder.create(inflater, parent);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bindTo(imageResources[position]);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return imageResources.length;
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        private GlideItemBinding binding;

        public ItemViewHolder(GlideItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public static ItemViewHolder create(LayoutInflater inflater, ViewGroup parent) {
            GlideItemBinding binding = GlideItemBinding.inflate(inflater, parent, false);
            return new ItemViewHolder(binding);
        }

        public void bindTo(@DrawableRes int imageRes) {
            ItemViewModel vm = new ItemViewModel(imageRes);
            binding.setViewModel(vm);
        }
    }
}
